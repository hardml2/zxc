FROM python:3.8-slim-buster

ARG SECRET_NUMBER_ARG

ENV SECRET_NUMBER=$SECRET_NUMBER_ARG

WORKDIR /app
COPY . /app

RUN python3 -m pip install flask

ENTRYPOINT ["python3"]

CMD ["app.py"]