from flask import Flask, jsonify

import time
import os

app = Flask(__name__)


@app.route('/return_secret_number')
def return_secret_number():
    time.sleep(1)
    return jsonify(secret_number=os.environ['SECRET_NUMBER'])


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=2000)